//
//  AppDelegate.h
//  Primes
//
//  Created by Dmitry on 19.09.15.
//  Copyright (c) 2015 Dmitry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


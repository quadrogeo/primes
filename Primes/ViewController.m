//
//  ViewController.m
//  Primes
//
//  Created by Dmitry on 19.09.15.
//  Copyright (c) 2015 Dmitry. All rights reserved.
//

#import "ViewController.h"

#define TICK   NSDate *startTime = [NSDate date]
#define TOCK   NSLog(@"Time: %f", -[startTime timeIntervalSinceNow])

@interface ViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *countTextField;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

- (IBAction)generateBtnClicked:(id)sender;

@end

@implementation ViewController {

    NSMutableArray *primes;
    NSInteger count;
    char *sieve;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.countTextField setReturnKeyType:UIReturnKeyDone];
    [self.countTextField setDelegate:self];
    
    primes = [[NSMutableArray alloc] init];
    sieve = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)generatePrimes {
    
    count = [self.countTextField.text integerValue];
    if (count == 0) {
        [primes removeAllObjects];
        return;
    }
    sieve = (char *)malloc(count);
    
    memset(&sieve[0], 1, count);

    sieve[1] = 0;
    for (int i=1; i*i<=count; i++) {
        if (sieve[i]) {
            for (int j=i*i; j<=count; j+=i)
                sieve[j] = 0;
        }
    }
    
    [primes removeAllObjects];
    for (int i=1; i<=count; i++) {
        if (sieve[i]) {
            [primes addObject:[NSNumber numberWithInteger:i]];
            
        }
    }

    free(sieve);
    sieve = nil;
}

- (IBAction)generateBtnClicked:(id)sender {

    [self.view endEditing:YES];
    [self.view setUserInteractionEnabled:NO];
    [self.activityIndicator startAnimating];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self generatePrimes];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            [self.activityIndicator stopAnimating];
            [self.view setUserInteractionEnabled:YES];
        });
    });

}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSCharacterSet *nonNumberSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    return ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0) || [string isEqualToString:@""];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [primes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PrimeCell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:@"PrimeCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    [cell.textLabel setText:[[primes objectAtIndex:indexPath.row] stringValue]];
    
    return cell;
}


@end
